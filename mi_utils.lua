function mi_load_scan(filename, is_annotation)
    -- open the file
    local file = torch.DiskFile(filename, 'r')
    file:binary() -- read in binary mode
    file:bigEndianEncoding() -- force Java-style endian
    -- read out headers
    local scan = {}
    local name_len = file:readInt()
    local name_bytes = file:readByte(name_len)
    scan.name = ''
    for i = 1, #name_bytes do
        scan.name = scan.name .. string.char(name_bytes[i])
    end
    scan.num_slices = file:readInt()
    scan.width = file:readInt()
    scan.height = file:readInt()
    scan.start_depth = file:readFloat()
    if is_annotation == nil then
        scan.ascending_depth = file:readByte() == 1
    end
    scan.slice_thickness = file:readFloat()
    scan.bytes_per_pixel = file:readInt()
    -- read out image data
    scan.slices = file:readByte(scan.bytes_per_pixel * scan.width * scan.height * scan.num_slices)
    scan.slices = torch.ByteTensor(scan.slices, 1, torch.LongStorage{scan.num_slices, scan.height, scan.width})
    -- close file
    file:close()
    return scan
end

function mi_load_annotation(filename)
    return mi_load_scan(filename, true)
end

function mi_str_split(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end

function mi_filter(array, filter)
	local keep = {}
	for _, v in ipairs(array) do
		if filter(v) then
			table.insert(keep, v)
		end
	end
	return keep
end

function mi_list_dataset(filename)
    local directories = {}
    -- open file for reading
    local file = torch.DiskFile(filename, 'r')
    file:quiet()
    while file:hasError() ~= true do
        local line = file:readString('*l')
        if line:len() > 0 then
            local split = mi_str_split(line, ' ')
            directories[#directories + 1] = {
                name = split[1],
                annotation_count = tonumber(split[2]),
		num_slices = tonumber(split[3]),
		slice_thickness = tonumber(split[4])
            }
        end
    end
    file:close()
    return directories
end

function mi_load_dataset(opt, filter)

    -- load the dataset
    local dataset_list = mi_list_dataset(opt.dataset_file)
    -- apply filter to dataset_list
    dataset_list = mi_filter(dataset_list, filter)

    -- split dataset into training and test sets
    dataset = {
        train = {},
        test = {},
        root_dir = opt.dataset_root
    }
    local dataset_size = math.min(#dataset_list, opt.max_dataset_size)
    local train_last = math.floor(dataset_size * opt.dataset_split)
    for i = 1, dataset_size do
        if i <= train_last then
            table.insert(dataset.train, dataset_list[i])
        else
            table.insert(dataset.test, dataset_list[i])
        end
    end
    
    -- Knuth shuffle the datasets
    if opt.shuffle_datasets then
        shuffle(dataset.train)
        shuffle(dataset.test)
    end
    
    return dataset
end

function shuffle(t)
    local n = #t
    while n > 1 do
        local k = math.random(n)
        t[n], t[k] = t[k], t[n]
        n = n - 1
    end
    return t
end

-- function to zero out tensors
function zeroDataSize(data)
  if type(data) == 'table' then
    for i = 1, #data do
      data[i] = zeroDataSize(data[i])
    end
  elseif type(data) == 'userdata' then
    data = torch.Tensor():typeAs(data)
  end
  return data
end

-- Resize the output, gradInput, etc temporary tensors to zero (so that the on disk size is smaller)
function cleanupModel(node)
  local cache = {}
  if node.output ~= nil then
    cache.output = node.output
    node.output = zeroDataSize(node.output)
  end
  if node.gradInput ~= nil then
    cache.gradInput = node.gradInput
    node.gradInput = zeroDataSize(node.gradInput)
  end
  if node.finput ~= nil then
    cache.finput = node.finput
    node.finput = zeroDataSize(node.finput)
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      cache.modules = {}
      for i = 1, #node.modules do
        local child = node.modules[i]
        cache.modules[i] = cleanupModel(child)
      end
    end
  end
  collectgarbage()
  return cache
end

function restoreModel(node, cache)
  if node.output ~= nil then
    node.output = cache.output
  end
  if node.gradInput ~= nil then
    node.gradInput = cache.gradInput
  end
  if node.finput ~= nil then
    node.finput = cache.finput
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      for i = 1, #node.modules do
        restoreModel(node.modules[i], cache.modules[i])
      end
    end
  end
  collectgarbage()
end

function usageGPU(format)
    format = format or 'b'
    local freeB, totalB = cutorch.getMemoryUsage(cutorch.getDevice())
    if format == 'b' then
        return string.format("%d / %d bytes", (totalB - freeB), totalB)
    elseif format == 'kb' then
        return string.format("%d / %d kbytes", (totalB - freeB) / 1024, total / 1024)
    elseif format == 'mb' then
        return string.format("%.1f / %.1f mbytes", (totalB - freeB) / (1024*1024), totalB / (1024*1024))
    elseif format == 'gb' then
        return string.format("%.1f / %.1f gbytes", (totalB - freeB) / (1024*1024*1024), totalB / (1024*1024*1024))
    end
end
