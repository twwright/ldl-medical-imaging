-- prepare print function (depending on whether log is defined)
if not log then
  log = print
end

-- -----------------
-- LOAD DEPENDENCIES
-- -----------------

-- make sure we have what we need
require 'cunn'
require 'nngraph'
require 'optim'

require 'adadelta_wd' -- custom adadelta that has weight decay added
require 'WeightedBCECriterion' -- custom weighted binary cross-entropy criterion
require 'mi_utils' -- helper functions for loading scans/labels
 
-- -------------------
-- PREPARE THE DATASET
-- -------------------


-- LOAD THE DATASET LISTING
log('Loading dataset')
function filter(e)
    return opt.base_thickness % e.slice_thickness < 0.0001
end
local dataset = mi_load_dataset(opt, filter)
log('Training dataset: ' .. #dataset.train)
log('Testing dataset: '.. #dataset.test)
log('Loaded dataset!')

-- CREATE STORAGE FOR INPUTS
log('Initialising and setting loader')
local loader = require 'loader_mixed'
loader:init(opt)
loader:set(dataset.train)

-- ----------------
-- CREATE THE MODEL
-- ----------------

-- clear up CUDA model and criterion if they exist
model = nil
criterion = nil
collectgarbage()
log('Cleared CUDA model resources')

local builder = require 'modelBuilder'

function final_layers_func()
    assert(opt.use_nngraph, 'DAG net not compatible with nn.Sequential!')
    assert(opt.window_size >= 0 and opt.window_size <= 3, 'DAG net only compatible with window size [0, 3]')
    assert(opt.dag_skip_count <= opt.dag_conv_count, 'Cannot create more skips than there are conv layers!')
    
    -- initialise list to hold the layers we create so we can recombine them later (and we need the maps count)
    local layers = {
        { -- save the current layer
            layer = builder.prev_layer,
            maps = builder.dims.maps
        }
    }
    
    -- create the main conv stream
    for i = 1, opt.dag_conv_count do
        -- add new layers
        local new_maps = builder.dims.maps * 2
        builder:add_s_conv(new_maps, 3, 3, 1, 1, builder.default_nonlin)
        builder:add_s_conv(new_maps, 3, 3, 1, 1, builder.default_nonlin)
        builder:add_max_pool(nil, 2, 2)
        -- save the current layer
        layers[#layers + 1] = { layer = builder.prev_layer, maps = builder.dims.maps }
    end
    
    -- process the layers involved in the skip into prediction layers
    for i = 1, opt.dag_skip_count + 1 do -- number of layers involved is one more than the number of skips
        local l = layers[#layers + 1 - i]
        local temp = nn.SpatialConvolution(l.maps, 1, 1, 1, 1, 1, 0, 0)(l.layer)
        if opt.dag_join_model then
            print(string.format('Create layer %d without sigmoid', #layers + 1 - i))
            l.layer = temp
        else
            l.layer = nn.Sigmoid()(temp)
        end
        l.maps = 1
    end
    
    -- create skip layers
    local prev_skip = { layer = layers[#layers].layer, maps = layers[#layers].maps }
    for i = 1, opt.dag_skip_count do
        if opt.dag_join_model then
            print(string.format('Join layer %d with %d maps from previous skip', #layers - i, prev_skip.maps)) 
            prev_skip.layer = nn.JoinTable(2)({ layers[#layers - i].layer, nn.SpatialFullConvolution(prev_skip.maps, prev_skip.maps, 2, 2, 2, 2)(prev_skip.layer) })
            prev_skip.maps = prev_skip.maps + 1
        else
            prev_skip.layer = nn.CAddTable()({ layers[#layers - i].layer, nn.SpatialFullConvolution(1, 1, 2, 2, 2, 2)(prev_skip.layer) })
        end
    end
    
    -- if using the join model, need to conv down to a single map
    if opt.dag_join_model then
        print('Conv ' .. prev_skip.maps .. ' join channels down to 1') 
        prev_skip.layer = nn.SpatialConvolution(prev_skip.maps, 1, 1, 1, 1, 1, 0, 0)(prev_skip.layer)
        prev_skip.maps = 1
    end
    
    -- create final prediction layer (upsampled if necessary)
    if opt.prediction_upsample then
        local up = opt.prediction_upsample
        builder.prev_layer = nn.Sigmoid()(nn.SpatialFullConvolution(1, 1, up, up, up, up)(prev_skip.layer))
    else
        builder.prev_layer = nn.Sigmoid()(prev_skip.layer)
    end

end

model, criterion = builder:init(opt, final_layers_func)

builder:clear()

graph.dot(model.fg, 'DAG', 'DAG')
log('Criterion: ' .. torch.type(criterion))

-- get the model parameters
params, grad_params = model:getParameters()

log('Model and criterion created!')

-- test the model sizes
log('Inputs')
print(loader.batch_inputs:size())
local output = model:forward(loader.batch_inputs)
-- output sizes
local sizes = torch.IntTensor(loader.batch_inputs:dim(), model:size()):fill(0)
for i = 1, model:size() do
    local outputSize = model:get(i).output:size():size()
    for j = 1, outputSize do
        sizes[j][i] = model:get(i).output:size()[j]
    end
end
print(sizes)
output = nil

-- ------------------
-- TRAINING THE MODEL
-- ------------------

-- define the evaluation function
function feval(x)
    --log('Running feval..')
    if x ~= params then
        params:copy(x)
    end

    -- reset gradients
    grad_params:zero()

    -- evaluate training batch
    local output = model:forward(inputs)
    local loss = criterion:forward(output, labels)

    -- estimate dloss/dW
    local dloss_dw = criterion:backward(output, labels)
    model:backward(inputs, dloss_dw)

    -- return loss and gradients
    return loss,grad_params
end
log('Defined evaluation function!')

-- define a function for testing loss on test set
function test_loss(set)
    log('Testing on dataset of ' .. #set .. ' in batches of ' .. opt.batch_size)
    loader:set(set) -- initialise loader with the test set
    local loaded = 0
    local batch = 0
    local loss_sum = 0
    repeat
        -- load data into batch_inputs
        loaded, inputs, labels = loader:load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            -- force binary labels if set (any labels > 0 forced to 1)
            if opt.force_binary_labels then
                labels[labels:gt(0.5)] = 1
                labels[labels:le(0.5)] = 0
            end
            
            -- perform forward model and criterion pass
            local output = model:forward(inputs)
            local loss = criterion:forward(output, labels)
                
            -- accumulate loss
            loss_sum = loss_sum + loss
            batch = batch + 1
            if opt.batch_print_every > 0 and batch % opt.batch_print_every == 0 then
                log(string.format('Tested %5d batches (loss: %.3f)', batch, loss_sum / batch))
                collectgarbage()
            end
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    return loss_sum / batch
end
log('Defined test loss function')

-- define a function for evaluating performance
function evaluate(set)
    log('Evaluating performance on dataset of ' .. #set .. ' in batches of ' .. opt.batch_size)
    local loader = require 'loader_sequential'
    loader:init(opt)
    loader:set(dataset.test)

    -- Prepare evaluation
    local num_thresholds = opt.pr_thresholds
    local thresholds = torch.linspace(0, 1, num_thresholds)

    -- 4: (tp, fp, tn, fn), 5: precision, 6: recall, 7: fpr, 8: acc 
    local results = torch.FloatTensor(#loader.dataset, thresholds:size(1), 8):zero() 
    local evaluated_count = 0

    -- create an output tensor to compile the crops into for each slice
    local total_output = torch.FloatTensor(opt.size / opt.label_subsample, opt.size / opt.label_subsample):cuda()
    total_output:fill(0)

    if opt.evaluation_subsample then
      local s_in = opt.size / opt.label_subsample
      local s_out = s_in / opt.evaluation_subsample
      local pool_in = nn.View(1, s_in, s_in)()
      local pool_out = nn.View(s_out, s_out)(nn.SpatialMaxPooling(opt.evaluation_subsample, opt.evaluation_subsample)(pool_in))
      max_pool = nn.gModule({pool_in}, {pool_out}):float()
    end

    -- Evaluate testing dataset
    local time = os.clock()
    for i = 1, #loader.dataset do
      -- load the next example, but check if it has any nodules before we process it
      local loaded_windows = loader:next(i)
      if loaded_windows > 0 and loader.loaded.label:gt(0.5):sum() > 0 then
        -- evaluate the example
        local loaded = 0
        local batch = 0
        local crop = 0
        local depth = 1 -- start at the first slice
        repeat
          -- load a batch sequentially
          loaded, inputs, labels = loader:load()
          if loaded > 0 then
            -- evaluate training batch
            local outputs = model:forward(inputs)

            batch = batch + 1

            -- process batch to apply each crop to correct slice
            for j = 1, loaded do
              -- check if we have moved to the next slice (depth is 1-based, crop is 0-based)
              if crop >= (2 * opt.crops - 1) * (2 * opt.crops - 1) then
                -- evaluate the completed slice
                --cuda_label:copy(loader.loaded.label[depth])
                local y = loader.loaded.label[depth]:gt(0.5):float()
                if opt.evaluation_subsample then
                  y = max_pool:forward(y):clone()
                end
                local images = {y}
                for k = 1, thresholds:size(1) do
                    local y_hat = total_output:ge(thresholds[k]):float()
                    if opt.evaluation_subsample then
                      y_hat = max_pool:forward(y_hat):clone()
                    end
                    images[#images + 1] = y_hat
                    y_hat:mul(2):add(y)
                    results[i][k][1] = results[i][k][1] + y_hat:eq(3):sum() -- true positive: 1x2 + 1
                    results[i][k][2] = results[i][k][2] + y_hat:eq(2):sum() -- false positive: 1x2 + 0
                    results[i][k][3] = results[i][k][3] + y_hat:eq(0):sum() -- true negative: 0x2 + 0
                    results[i][k][4] = results[i][k][4] + y_hat:eq(1):sum() -- false negative: 0x2 + 1
                end
                if y:sum() > 0 and output_slices then
                  itorch.image(images)
                end
                -- move to the next slice
                depth = depth + 1
                crop = 0
                total_output:fill(0)
              end
              -- calculate crop dimensions
              local crop_size = (opt.size / opt.label_subsample) / opt.crops
              local crop_x = (crop % (2 * opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
              local crop_y = math.floor(crop / (2 * opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
              -- max the crop into the slice
              local total_output_cropped = total_output[{{crop_y + 1, crop_y + crop_size}, {crop_x + 1, crop_x + crop_size}}]
              total_output_cropped:cmax(outputs[j])
              --itorch.image(outputs[j])
              --itorch.image(total_output)
              -- increment crop for next time
              crop = crop + 1
            end
          end
        until loaded == 0

        -- calculate precision and recall if we processed any batches
        for j = 1, thresholds:size(1) do
            local tp = results[i][j][1]
            local fp = results[i][j][2]
            local tn = results[i][j][3]
            local fn = results[i][j][4]
            if fp == 0 then
              results[i][j][5] = 0
            else
              results[i][j][5] = tp / (tp + fp) -- precision = tp / (tp + fp)
            end
            results[i][j][6] = tp / (tp + fn) -- recall = tp / (tp + fn)
            results[i][j][7] = fp / (fp + tn) -- fpr
            results[i][j][8] = (tp + tn) / (tp + fp + tn + fn) -- accuracy
        end

        -- increment the evaluation count
        evaluated_count = evaluated_count + 1

        if opt.batch_print_every > 0 and i % opt.batch_print_every == 0 then
            log('Evaluated: ' .. evaluated_count .. ' / ' .. i)
        end
      end
    end
    
    -- take the mean in order to calculate metrics
    local mean_results = results:sum(1):squeeze() / evaluated_count -- take mean along the last axis

    -- adjust results to avoid NaNs
    local eps = 0.000001
    mean_results[mean_results:lt(eps)] = eps

    -- create a new tensor with room for F-score
    local res = torch.FloatTensor(mean_results:size(1), mean_results:size(2) + 1)
    res[{{}, {1, mean_results:size(2)}}]:copy(mean_results)


    -- calculate metrics
    local precision = res[{{}, 5}]
    local recall = res[{{}, 6}]
    local fpr = res[{{}, 7}]
    local acc = res[{{}, 8}]

    local fscore = precision:clone():cmul(recall) -- fscore = 2 * ((precision*recall)/(precision+recall))
    local temp_denom = torch.add(precision, recall)
    fscore:cdiv(temp_denom):mul(2)
    res[{{}, 9}]:copy(fscore)
    
    -- store the results globally
    evaluation = res
    
    -- find the best f-score to print
    local best = 1
    for i = 2, thresholds:size(1) do
        if res[{i, 9}] > res[{best, 9}] then best = i end
    end
    log(string.format('Precision %.3f, Recall %.3f, F-score %.3f @ Threshold %.3f (%d)', res[{best, 5}], res[{best, 6}], res[{best, 9}], thresholds[best], i))
    print(evaluation)
    log(string.format('Evaluation complete in %.2fs', os.clock() - time))
end

-- perform training
losses = {}
test_losses = {}
local config = { weightDecay = opt.weight_decay or 0 }
log('Starting training')
log('Training on dataset of ' .. #dataset.train .. ' in batches of ' .. opt.batch_size .. ', ' .. opt.epochs .. ' epochs')
model.verbose = false

for epoch = 1, opt.epochs do
    
    -- shuffle the datasets again
    if opt.shuffle_datasets then
        shuffle(dataset.train)
        shuffle(dataset.test)
    end
    
    -- perform iteration over training set
    loader:set(dataset.train) -- initialise loader with the training set
    local start_time = os.clock()
    
    local loaded = 0
    local batch = 0
    local loss_sum = 0
    repeat
        -- load data into batch_inputs
        loaded, inputs, labels = loader:load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            -- force binary labels if set (any labels > 0 forced to 1)
            if opt.force_binary_labels then
                labels[labels:gt(0.5)] = 1
                labels[labels:le(0.5)] = 0
            end
            
            -- perform optimisation
            local _, loss = optim.adadeltawd(feval, params, config)
                
            -- record losses
            losses[#losses + 1] = loss[1]
            loss_sum = loss_sum + loss[1]
            batch = batch + 1
            if opt.batch_print_every > 0 and batch % opt.batch_print_every == 0 then
                log(string.format('Trained %5d batches (loss: %.3f)', batch, loss_sum / batch))
                collectgarbage()
            end
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    local training_time = os.clock() - start_time
    local epoch_loss = loss_sum / batch

    -- compute the loss over the test set
    start_time = os.clock()
    test_losses[#losses] = test_loss(dataset.test)
    local test_time = os.clock() - start_time
    
    -- evaluate performance on the test set
    if opt.evaluate and (epoch % opt.save_every == 0 or epoch == opt.epochs) then
        evaluate(dataset.test)
    end
    
    if epoch % opt.print_every == 0 then
        log(string.format("Epoch %4d complete in %5.1fs / %5.1fs. Loss: %10.6f, Test Loss: %10.6f", epoch, training_time, test_time, epoch_loss, test_losses[#losses]))
        log(string.format("GPU Usage: %s", usageGPU('gb')))
        collectgarbage()
    end

    if epoch % opt.save_every == 0 or epoch == opt.epochs then
      log('Saving model checkpoint ' .. epoch)
      local cache = cleanupModel(model)
      model:clearState()
      torch.save(job_dir .. 'model_' .. epoch .. '.t7', model)
      restoreModel(model, cache)
      log('Saving losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'losses_' .. epoch .. '.t7', losses)
      log('Saving test losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'test_losses_' .. epoch .. '.t7', test_losses)
      if opt.evaluate then
          torch.save(job_dir .. 'evaluation_' .. epoch .. '.t7', evaluation)
      end
      collectgarbage()
    end
end

model = nil
criterion = nil
output = nil
inputs = nil
labels = nil
collectgarbage()
print('Final GPU Usage: ' .. usageGPU('gb'))

log('Done!')