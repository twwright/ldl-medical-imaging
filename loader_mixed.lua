-- CREATE STORAGE FOR INPUTS
local loader = {}

function loader:init(opt)
    self.opt = opt
    
    -- create subsampling
    local input_sub = opt.input_subsample or 1
    local label_sub = opt.label_subsample or 1
    self.mean_subsample = nn.VolumetricAveragePooling(input_sub, input_sub, input_sub, input_sub, input_sub, input_sub)
    self.mean_subsample:float()
    -- labels will be subsampled by input_sub in depth dimension, and label_sub in other dimensions!
    self.max_subsample = nn.VolumetricMaxPooling(input_sub, label_sub, label_sub, input_sub, label_sub, label_sub)
    self.max_subsample:float()

    -- create tensors to load inputs and labels into
    local depth = opt.window_size * 2 + 1 -- middle slice plus window_size on each side
    local input_size = (opt.size / input_sub) / opt.crops
    self.batch_inputs = torch.FloatTensor(opt.batch_size, 1, depth, input_size, input_size)
    self.batch_inputs = self.batch_inputs:cuda()
    if self.opt.load_labels then
        local label_size = (opt.size / label_sub) / opt.crops
        self.batch_labels = torch.FloatTensor(opt.batch_size, 1, 1, label_size, label_size)
        self.batch_labels = self.batch_labels:cuda()
    end
end

-- DEFINE FUNCTIONS TO LOAD INPUTS
function loader:load_input(dir_name, annotation_count)
    -- load the scan, calculate the slice copying factor (every slice, every second, every fourth etc.) and generate copy indices
    local scan = mi_load_scan(dir_name .. '/slices')
    local factor = self.opt.base_thickness / scan.slice_thickness
    local indices = torch.range(1, scan.slices:size(1), factor)
    
    -- calculate crops
    self.total_crops = (2 * opt.crops - 1) * (2 * opt.crops - 1)
    
    -- scan, copy correct slices and mean subsample
    local scan_input = torch.FloatTensor(1, math.ceil(scan.slices:size(1) / factor), scan.slices:size(2), scan.slices:size(2))
    if factor == 1 then
        scan_input:copy(scan.slices)
    else 
        for i = 1, math.ceil(scan.slices:size(1) / factor) do
            scan_input[1][i]:copy(scan.slices[indices[i]])
        end
    end
    local pooled_input = self.mean_subsample:forward(scan_input)
    local padded_input = torch.FloatTensor(pooled_input:size(2) + (self.opt.window_size * 2), pooled_input:size(3), pooled_input:size(4)):zero()
    padded_input[{{self.opt.window_size + 1, pooled_input:size(2) + self.opt.window_size}}]:copy(pooled_input[1])
    
    -- label, copy correct slices, divide by annotation count to create average, max subsample
    if self.opt.load_labels then
        local label = mi_load_annotation(dir_name .. '/annotations_sum')
        local label_output = torch.FloatTensor(1, math.ceil(label.slices:size(1) / factor), label.slices:size(2), label.slices:size(2))
        if factor == 1 then
            label_output:copy(label.slices)
        else 
            for i = 1, math.ceil(label.slices:size(1) / factor) do
                label_output[1][i]:copy(label.slices[indices[i]])
            end
        end
        label_output:div(annotation_count)
        local pooled_output = self.max_subsample:forward(label_output)
        return padded_input, pooled_output[1]:clone() -- subsampling required a batch dimension, we don't want that
    end
    return padded_input -- if we reach here load_labels must not be set
end

-- assign a new dataset to the loader
function loader:set(set)
    self.p_dataset = 0
    self.p_load = 0 -- p_load points to the last index that was loaded from the input (p_load + 1 is start of next window to load)
    self.loaded = {}
    self.windows = {}
    self.dataset = set
    -- skipping counter
    self.skip_count = 0
end

-- initial state
function loader:load()
    local p_batch = 0 -- p_batch points to the last index that was filled in batch (p_batch + 1 is next spot to load)
    -- while (batch not full) AND (dataset has data OR loaded input has data)
    while p_batch < self.opt.batch_size and (self.p_dataset < #self.dataset or self.p_load < #self.windows) do
        -- if loaded input exhausted
        if self.p_load == #self.windows then
            -- load input batch and create window list
            self.loaded = {}
            self.windows = {}
            log(string.format('Loading %d inputs...', self.opt.load_batch_size))
            for i = 1, self.opt.load_batch_size do
                local input, label
                repeat
                    self.p_dataset = self.p_dataset + 1
                    if self.p_dataset > #self.dataset then
                        break
                    end
                    input, label = self:load_input(self.opt.dataset_root .. self.dataset[self.p_dataset].name, self.dataset[self.p_dataset].annotation_count)
                until not self.opt.load_labels or not self.opt.skip_negative_labels or label:gt(0.5):sum() > 0
                -- check dataset point again, we might have just broken from the repeat-until
                if self.p_dataset > #self.dataset then
                    break
                end
                self.loaded[i] = { input = input, label = label }
                for j = 1, self.loaded[i].input:size(1) - self.opt.window_size * 2 do -- add a entry to list for each window in input
                    for k = 1, self.total_crops do
                        table.insert(self.windows, {i, j, k})
                    end
                end
            end
            log(string.format('Loaded %d new windows (%d inputs)', #self.windows, #self.loaded))
            -- shuffle window list
            shuffle(self.windows)
            self.p_load = 0
        end
        -- fill batch from input
        while p_batch < self.opt.batch_size and self.p_load < #self.windows do
            self.p_load = self.p_load + 1
            p_batch = p_batch + 1
            local input = self.windows[self.p_load][1]
            local window = self.windows[self.p_load][2]
            -- determine which window we want
            local window = self.windows[self.p_load][2]
            local crop_index = self.windows[self.p_load][3] - 1 -- convert to a 0-based index
            local crop_size, crop_x, crop_y
            -- handle copying of labels/inputs, and skipping if negative
            local skip = false
            if self.opt.load_labels then
                -- calculate crop dimensions for the label
                crop_size = self.loaded[input].label:size(2) / self.opt.crops
                crop_x = (crop_index % (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
                crop_y = math.floor(crop_index / (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
                local crop = self.loaded[input].label[{
                    window,
                    {crop_y + 1, crop_y + crop_size},
                    {crop_x + 1, crop_x + crop_size}}]
                if self.opt.skip_negative_labels and crop:sum() == 0 then
                    skip = true
                    self.skip_count = self.skip_count + 1
                    -- override skip if we have filled the skip quota
                    if self.skip_count > self.opt.skip_negative_labels then
                        skip = false
                        self.skip_count = 0
                    end
                end
                if not skip then
                    self.batch_labels[p_batch]:copy(crop)
                end
            end
            
            if not skip then
                -- copy the input if we aren't skipping
                crop_size = self.loaded[input].input:size(2) / self.opt.crops
                crop_x = (crop_index % (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
                crop_y = math.floor(crop_index / (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
                self.batch_inputs[p_batch]:copy(self.loaded[input].input[{
                        {window, window + self.opt.window_size * 2},
                        {crop_y + 1, crop_y + crop_size},
                        {crop_x + 1, crop_x + crop_size}}])
                
            end
            
            -- move batch point back if we just skipped
            if skip then
                p_batch = p_batch - 1
            end
        end
    end
    if p_batch > 0 then
        if self.opt.load_labels then
            return p_batch, self.batch_inputs[{{1, p_batch}}], self.batch_labels[{{1, p_batch}}]
        else
            return p_batch, self.batch_inputs[{{1, p_batch}}]
        end
    else
        return 0
    end
end

return loader