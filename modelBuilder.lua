require 'nn'
require 'cunn'

local builder = {}

function builder:init(opt, final_layers_func)
    -- input sizes
    self.in_depth = opt.window_size * 2 + 1
    self.in_height = (opt.size / opt.input_subsample) / opt.crops
    self.in_width = (opt.size / opt.input_subsample) / opt.crops

    -- set the seed
    if opt.seed then
        torch.manualSeed(opt.seed)
    end
    
    
    if opt.use_nngraph then
        require 'nngraph'
        self.input_layer = nn.Identity()()
        self.prev_layer = self.input_layer
    else
        self.model = nn.Sequential()
    end
    
    self.dims = {
        maps = 1,
        depth = self.in_depth,
        height = self.in_height,
        width = self.in_width
    }
    
    builder.default_nonlin = nn.Sigmoid
    if opt.nonlinearity == 'relu' then default_nonlin = nn.ReLU end
    if opt.nonlinearity == 'tanh' then default_nonlin = nn.Tanh end
    if opt.nonlinearity == 'elu' then default_nonline = nn.ELU end
    
    self.opt = opt
    
    -- build the model
    -- create layers as defined in opts
    for i, l in ipairs(opt.model) do
        -- determine nonlin
        local nonlin = builder.default_nonlin
        if l.nonlin == 'none' then
            nonlin = nil
        elseif l.nonlin == 'relu' then
            nonlin = nn.ReLU
        elseif l.nonlin == 'tanh' then
            nonlin = nn.Tanh
        elseif l.nonlin == 'sigmoid' then
            nonlin = nn.Sigmoid
        end
        -- add layer
        if l.type == 'vconv' then
            self:add_v_conv(l.maps, l.kernel[1], l.kernel[2], l.kernel[3], l.pad[1], l.pad[2], l.pad[3], nonlin)
        elseif l.type == 'conv' then
            self:add_s_conv(l.maps, l.kernel[1], l.kernel[2], l.pad[1], l.pad[2], nonlin)
        elseif l.type == 'pool' then
            if #l.kernel == 3 then
                self:add_max_pool(l.kernel[1], l.kernel[2], l.kernel[3])
            else
                self:add_max_pool(nil, l.kernel[1], l.kernel[2])
            end
        elseif l.type == 'deconv' then
            local stride = l.stride or l.kernel
            self:add_deconv(l.maps, l.kernel[1], l.kernel[2], stride[1], stride[2], nonlin)
        elseif l.type == 'reshape' then
            if opt.use_nngraph then
                self.prev_layer = nn.Reshape(self.dims.maps, self.dims.height, self.dims.width)(self.prev_layer)
            else
                self.model:add(nn.Reshape(self.dims.maps, self.dims.height, self.dims.width))
            end
        end
    end

    if final_layers_func then
        final_layers_func()
    end

    -- finalise model
    if self.opt.use_nngraph then
        self.model = nn.gModule({self.input_layer}, {self.prev_layer})
    end
    self.model:cuda()
    
    -- create the criterion
    if self.opt.bce_weight then
        self.criterion = nn.WeightedBCECriterion(self.opt.bce_weight)
    else
        self.criterion = nn.BCECriterion()
    end
    self.criterion:cuda()
    
    return self.model, self.criterion 
end

function builder:clear()
    self.model = nil
    self.criterion = nil
end

-- helper functions to add layers
function builder:add_batch_norm(maps, d, h, w)
    if self.opt.use_nngraph then
        if d then
            self.prev_layer = nn.View(-1, self.dims.maps, d * h, w)(self.prev_layer)
        end
        self.prev_layer = nn.SpatialBatchNormalization(self.dims.maps)(self.prev_layer)
        if d then
            self.prev_layer = nn.View(-1, self.dims.maps, d, h, w)(self.prev_layer)
        end
    else
        if d then
            self.model:add(nn.View(-1, self.dims.maps, d * h, w))
        end
        self.model:add(nn.SpatialBatchNormalization(self.dims.maps))
        if d then
            self.model:add(nn.View(-1, self.dims.maps, d, h, w))
        end
    end
end

function builder:add_max_pool(kern_d, kern_h, kern_w)
    if self.opt.use_nngraph then
        if kern_d then
            self.prev_layer = nn.VolumetricMaxPooling(kern_d, kern_h, kern_w)(self.prev_layer)
            self.dims.depth = math.floor(self.dims.depth / kern_d)
        else
            self.prev_layer = nn.SpatialMaxPooling(kern_h, kern_w)(self.prev_layer)
        end
    else
        if kern_d then
            self.model:add(nn.VolumetricMaxPooling(kern_d, kern_h, kern_w))
            self.dims.depth = math.floor(self.dims.depth / kern_d)
        else
            self.model:add(nn.SpatialMaxPooling(kern_h, kern_w))
        end
    end
    self.dims.width = math.floor(self.dims.width / kern_h)
    self.dims.height = math.floor(self.dims.height / kern_w)
end

function builder:add_s_conv(out_maps, kern_h, kern_w, pad_h, pad_w, nonlin)
    pad_h = pad_h or 0
    pad_w = pad_w or 0
    -- add convolution layer
    if self.opt.use_nngraph then
        self.prev_layer = nn.SpatialConvolution(self.dims.maps, out_maps, kern_h, kern_w, 1, 1, pad_h, pad_w)(self.prev_layer)
    else
        self.model:add(nn.SpatialConvolution(self.dims.maps, out_maps, kern_h, kern_w, 1, 1, pad_h, pad_w))
    end
    -- record the new dimensions 
    self.dims.height = self.dims.height + 2 * pad_h - kern_h + 1
    self.dims.width = self.dims.width + 2 * pad_w - kern_w + 1
    self.dims.maps = out_maps
    -- apply batch norm layers
    if self.opt.batch_norm == true then
        self:add_batch_norm(out_maps, nil, self.dims.height, self.dims.width)
    end
    -- add nonlinearity
    if nonlin then
        if self.opt.use_nngraph then
            self.prev_layer = nonlin()(self.prev_layer)
        else
            self.model:add(nonlin())
        end
    end
end

function builder:add_v_conv(out_maps, kern_d, kern_h, kern_w, pad_d, pad_h, pad_w, nonlin)
    pad_d = pad_d or 0
    pad_h = pad_h or 0
    pad_w = pad_w or 0
    -- add convolution layer
    if self.opt.use_nngraph then
        self.prev_layer = nn.VolumetricConvolution(self.dims.maps, out_maps, kern_d, kern_h, kern_w, 1, 1, 1, pad_d, pad_h, pad_w)(self.prev_layer)
    else
        self.model:add(nn.VolumetricConvolution(self.dims.maps, out_maps, kern_d, kern_h, kern_w, 1, 1, 1, pad_d, pad_h, pad_w))
    end
    -- record the new dimensions
    self.dims.depth = self.dims.depth + 2 * pad_d - kern_d + 1
    self.dims.height = self.dims.height + 2 * pad_h - kern_h + 1
    self.dims.width = self.dims.width + 2 * pad_w - kern_w + 1
    self.dims.maps = out_maps
    -- apply batch norm layers
    if opt.batch_norm == true then
        self:add_batch_norm(out_maps, self.dims.depth, self.dims.height, self.dims.width)
    end
    -- add nonlinearity
    if nonlin then
        if self.opt.use_nngraph then
            self.prev_layer = nonlin()(self.prev_layer)
        else
            self.model:add(nonlin())
        end
    end
end

-- spatial deconvolution
function builder:add_deconv(out_maps, kern_h, kern_w, stride_h, stride_w, nonlin)
    stride_h = stride_h or 1
    stride_w = stride_w or 1
    -- add convolution layer
    if self.opt.use_nngraph then
        self.prev_layer = nn.SpatialFullConvolution(self.dims.maps, out_maps, kern_h, kern_w, stride_h, stride_w)(self.prev_layer)
    else
        self.model:add(nn.SpatialFullConvolution(self.dims.maps, out_maps, kern_h, kern_w, stride_h, stride_w))
    end
    -- record changes to dimensions
    self.dims.height = (self.dims.height - 1) * stride_h + kern_h
    self.dims.width = (self.dims.width - 1) * stride_w + kern_w
    self.dims.maps = out_maps
    -- add batch norm
    if self.opt.batch_norm == true then
        self:add_batch_norm(out_maps, nil, self.dims.height, self.dims.width)
    end
    -- add nonlinearity
    if nonlin then
        if self.opt.use_nngraph then
            self.prev_layer = nonlin()(self.prev_layer)
        else
            self.model:add(nonlin())
        end
    end
end

return builder