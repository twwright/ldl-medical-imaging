require 'nn'
local nninit = require 'nninit'

local models = {}


-- deconv net
models.deconv_unpooling = function(opt)
    
    local Conv = nn.SpatialConvolution
    local BatchNorm = nn.SpatialBatchNormalization
    local ReLU = nn.ReLU
    local FullConv = nn.SpatialFullConvolution
    local Pooling = nn.SpatialMaxPooling
    local Unpooling = nn.SpatialMaxUnpooling
    
    local net = nn.Sequential()
    
    local ConvModule = function(net, maps, mapsTo, pretrain)
        local conv = Conv(maps, mapsTo, 3,3, 1,1, 1,1)
        if pretrain then
            conv.weight:copy(pretrain.weight)
            conv.bias:copy(pretrain.bias)
        end
        
        net:add(conv)
            :add(BatchNorm(mapsTo))
            :add(ReLU(true))
    end
    
    local FullConvModule = function(net, maps, mapsTo)
        net:add(FullConv(maps, mapsTo, 3,3, 1,1, 1,1))
            :add(BatchNorm(mapsTo))
            :add(ReLU(true))
    end
        
    assert(opt.inputDims, 'input dimensions not defined!')
    assert(opt.labelDims, 'label dimensions not defined!')
    
    -- load VGG
    local pretrainLayers = {}
    if opt.pretrainNet then
        require 'loadcaffe'
        require 'nn'
        local pretrain = loadcaffe.load(opt.pretrainNet .. '_deploy.prototxt', opt.pretrainNet .. '.caffemodel', 'nn')
        for _, i in ipairs({1, 3, 6, 8, 11, 13, 15, 18, 20, 22}) do
            table.insert(pretrainLayers, pretrain.modules[i])
        end
    end
    
    local pools = {
        Pooling(2,2,2,2),
        Pooling(2,2,2,2),
        Pooling(2,2,2,2)
    }
    
    -- conv stack
    ConvModule(net, opt.inputDims[1], 64, pretrainLayers[1])
    ConvModule(net, 64, 64, pretrainLayers[2])
    net:add(pools[1])
    ConvModule(net, 64, 128, pretrainLayers[3])
    ConvModule(net, 128, 128, pretrainLayers[4])
    net:add(pools[2])
    ConvModule(net, 128, 256, pretrainLayers[5])
    ConvModule(net, 256, 256, pretrainLayers[6])
    ConvModule(net, 256, 256, pretrainLayers[7])
    net:add(pools[3])
    ConvModule(net, 256, 512, pretrainLayers[8])
    ConvModule(net, 512, 512, pretrainLayers[9])
    ConvModule(net, 512, 512, pretrainLayers[10])
    
    -- deconv stack
    FullConvModule(net, 512, 512)
    FullConvModule(net, 512, 512)
    FullConvModule(net, 512, 256)
    net:add(Unpooling(pools[3]))
    FullConvModule(net, 256, 256)
    FullConvModule(net, 256, 256)
    FullConvModule(net, 256, 128)
    net:add(Unpooling(pools[2]))
    FullConvModule(net, 128, 128)
    FullConvModule(net, 128, 64)
    net:add(Unpooling(pools[1]))
    FullConvModule(net, 64, 64)
    
    -- output
    net:add(FullConv(64, 1, 3,3, 1,1, 1,1))
    net:add(nn.Sigmoid())
    
    net:float()
    
    return net
end

-- deconv net
models.vol_deconv = function(opt)
    
    local Seq = nn.Sequential
    local ReLU = nn.ReLU
    local VolBatchNorm = nn.VolumetricBatchNormalization
    local SpatBatchNorm = nn.SpatialBatchNormalization

    local function VolConv(...)
      return nn.VolumetricConvolution(...)
        :init('weight', nninit.kaiming, {dist = 'normal', gain = 'relu'})
        :init('bias', nninit.constant, 0)
    end

    local function SpatConv(...)
      return nn.SpatialConvolution(...)
        :init('weight', nninit.kaiming, {dist = 'normal', gain = 'relu'})
        :init('bias', nninit.constant, 0)
    end

    local function SpatFullConv(...)
      return nn.SpatialFullConvolution(...)
        :init('weight', nninit.kaiming, {dist = 'normal', gain = 'relu'})
        :init('bias', nninit.constant, 0)
    end
    
    local function SpatConvModule(maps, toMaps, kern, stride, pad)
      stride = stride or 1
      pad = pad or 1
      kern = kern or 3
      net
        :add(SpatConv(maps, toMaps, kern,kern, stride, stride, pad, pad))
        :add(SpatBatchNorm(toMaps))
        :add(ReLU(true))
    end
    
    local function VolConvModule(maps, toMaps, kern, stride, pad)
      stride = stride or 1
      pad = pad or 1
      kern = kern or 3
      net
        :add(VolConv(maps, toMaps, kern,kern,kern, stride, stride, stride, pad, pad, pad))
        :add(VolBatchNorm(toMaps))
        :add(ReLU(true))
    end
    
    local function SpatFullConvModule(maps, toMaps, kern, stride, pad)
      stride = stride or 1
      pad = pad or 1
      kern = kern or 3
      net
        :add(SpatFullConv(maps, toMaps, kern, kern, stride, stride, pad, pad))
        :add(SpatBatchNorm(toMaps))
        :add(ReLU(true))
    end
   
    net = Seq() 
    
    VolConvModule(opt.inputDims[1], 64)
    VolConvModule(64, 64, 2, 2, 0) -- 1/2
    
    VolConvModule(64, 128)
    VolConvModule(128, 128, 2, 2, 0) -- 1/4

    VolConvModule(128, 256)
    VolConvModule(256, 256, 2, 2, 0) -- 1/8

    -- remove depth dimension
    net:add(nn.Squeeze(2, 4))
    
    SpatFullConvModule(256, 256, 2, 2, 0)
    SpatFullConvModule(256, 128) -- 1/4
    
    SpatFullConvModule(128, 128, 2, 2, 0)
    SpatFullConvModule(128, 64) -- 1/2
    
    SpatFullConvModule(64, 64, 2, 2, 0)
    
    -- output
    net:add(SpatFullConv(64, 1, 3,3, 1,1, 1,1))
    net:add(nn.Sigmoid())
    
    net:float()
    
    return net
end

models.context_net = function(opt)

   local Seq = nn.Sequential
   local ReLU = nn.ReLU
   local SpatBatchNorm = nn.SpatialBatchNormalization
   local function SpatConv(...)
      return nn.SpatialDilatedConvolution(...)
        --:init('weight', nninit.kaiming, {dist = 'normal', gain = 'relu'})
        --:init('bias', nninit.constant, 0)
   end
    
   local function ConvModule(net, maps, toMaps, dilate)
       net
        :add(SpatConv(maps, toMaps, 3,3, 1,1, dilate,dilate, dilate,dilate))
        :add(SpatBatchNorm(toMaps))
        :add(ReLU(true))
   end

    local net = Seq()

    local m = opt.maps
    
    ConvModule(net, opt.inputDims[1], m, 1)
    ConvModule(net, m, m, 1)
    ConvModule(net, m, m, 2)
    ConvModule(net, m, m, 4)
    ConvModule(net, m, m, 8)
    ConvModule(net, m, m, 16)
    ConvModule(net, m, m, 1)
    
    net:add(SpatConv(m, 1, 3,3, 1,1, 1,1, 1,1))
    net:add(nn.Sigmoid())
    
    return net
end

models.deconv_context = function(opt)
    local Conv = nn.SpatialDilatedConvolution
    local BatchNorm = nn.SpatialBatchNormalization
    local ReLU = nn.ReLU
    local FullConv = nn.SpatialFullConvolution
    local Pooling = nn.SpatialMaxPooling
    local Unpooling = nn.SpatialMaxUnpooling
    
    local net = nn.Sequential()
    
    local ConvModule = function(net, maps, mapsTo, pretrain, dilate)
        dilate = dilate or 1
        local conv = Conv(maps, mapsTo, 3,3, 1,1, dilate,dilate, dilate,dilate)
        if pretrain then
            conv.weight:copy(pretrain.weight)
            conv.bias:copy(pretrain.bias)
        end
        
        net:add(conv)
            :add(BatchNorm(mapsTo))
            :add(ReLU(true))
    end
    
    local FullConvModule = function(net, maps, mapsTo)
        net:add(FullConv(maps, mapsTo, 3,3, 1,1, 1,1))
            :add(BatchNorm(mapsTo))
            :add(ReLU(true))
    end
        
    assert(opt.inputDims, 'input dimensions not defined!')
    assert(opt.labelDims, 'label dimensions not defined!')
    
    -- load VGG
    local pretrainLayers = {}
    if opt.pretrainNet then
        require 'loadcaffe'
        require 'nn'
        local pretrain = loadcaffe.load(opt.pretrainNet .. '_deploy.prototxt', opt.pretrainNet .. '.caffemodel', 'nn')
        for _, i in ipairs({1, 3, 6, 8, 11, 13, 15, 18, 20, 22}) do
            table.insert(pretrainLayers, pretrain.modules[i])
        end
    end
    
    local pools = {
        Pooling(2,2,2,2),
        Pooling(2,2,2,2),
        Pooling(2,2,2,2)
    }
    
    local depth = opt.vgg_depth or 1
    
    -- conv stack
    ConvModule(net, opt.inputDims[1], 64, pretrainLayers[1])
    ConvModule(net, 64, 64, pretrainLayers[2])
    net:add(pools[1]) -- 160 -> 80
    
    if depth > 1 then
    ConvModule(net, 64, 128, pretrainLayers[3])
    ConvModule(net, 128, 128, pretrainLayers[4])
    net:add(pools[2]) -- 80 -> 40
    end
    
    if depth > 2 then
        ConvModule(net, 128, 256, pretrainLayers[5])
        ConvModule(net, 256, 256, pretrainLayers[6])
        ConvModule(net, 256, 256, pretrainLayers[7])
        net:add(pools[3]) -- 40 -> 20
    end
    
    -- add context net
    local possibleMaps = { 128, 256, 512 }
    local m = possibleMaps[depth]
    ConvModule(net, m/2, m, nil, 1)
    ConvModule(net, m, m, nil, 1)
    ConvModule(net, m, m, nil, 2)
    ConvModule(net, m, m, nil, 4)
    ConvModule(net, m, m, nil, 8)
    ConvModule(net, m, m, nil, 16)
    ConvModule(net, m, m/2, nil, 1)
    
    
    -- deconv stack
    if depth > 2 then
        net:add(Unpooling(pools[3]))
        FullConvModule(net, 256, 256)
        FullConvModule(net, 256, 256)
        FullConvModule(net, 256, 128)
    end
    
    if depth > 1 then
        net:add(Unpooling(pools[2]))
        FullConvModule(net, 128, 128)
        FullConvModule(net, 128, 64)
    end
    
    net:add(Unpooling(pools[1]))
    FullConvModule(net, 64, 64)
    
    -- output
    net:add(FullConv(64, 1, 3,3, 1,1, 1,1))
    net:add(nn.Sigmoid())
    
    net:float()
    
    return net
end

return models