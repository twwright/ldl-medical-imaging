-- prepare print function (depending on whether log is defined)
if not log then
  log = print
end

-- -----------------
-- LOAD DEPENDENCIES
-- -----------------

-- make sure we have what we need
require 'cunn'
require 'nngraph'
require 'optim'

require 'WeightedBCECriterion' -- custom weighted binary cross-entropy criterion
require 'mi_utils' -- helper functions for loading scans/labels

-- -------------------
-- PREPARE THE DATASET
-- -------------------


-- LOAD THE DATASET LISTING
log('Loading dataset')
function filter(e)
    return opt.base_thickness % e.slice_thickness < 0.0001
end
local dataset = mi_load_dataset(opt, filter)
log('Training dataset: ' .. #dataset.train)
log('Testing dataset: '.. #dataset.test)
log('Loaded dataset!')

-- CREATE STORAGE FOR INPUTS
log('Initialising and setting loader')
local loader = require 'loader_mixed'
loader:init(opt)
loader:set(dataset.train)

-- ----------------
-- CREATE THE MODEL
-- ----------------

-- clear up CUDA model and criterion if they exist
model = nil
criterion = nil
collectgarbage()
log('Cleared CUDA model resources')

local builder = require 'modelBuilder'

function final_layers_func()
    if opt.use_nngraph then
        builder.prev_layer = nn.Sigmoid()(builder.prev_layer)
    else
        builder.model:add(nn.Sigmoid())
    end
end

model, criterion = builder:init(opt, final_layers_func)

-- get the model parameters
params, grad_params = model:getParameters()

log('Model and criterion created!')

-- test the model sizes
log('Inputs')
print(loader.batch_inputs:size())
model.verbose = true
local output = model:forward(loader.batch_inputs)
-- output sizes
local sizes = torch.IntTensor(loader.batch_inputs:dim(), model:size()):fill(0)
for i = 1, model:size() do
    local outputSize = model:get(i).output:size():size()
    for j = 1, outputSize do
        sizes[j][i] = model:get(i).output:size()[j]
    end
end
model.verbose = false
print(sizes)
output = nil

-- ------------------
-- TRAINING THE MODEL
-- ------------------

-- define the evaluation function
function feval(x)
    --log('Running feval..')
    if x ~= params then
        params:copy(x)
    end

    -- reset gradients
    grad_params:zero()

    -- evaluate training batch
    local output = model:forward(inputs)
    local loss = criterion:forward(output, labels)

    -- estimate dloss/dW
    local dloss_dw = criterion:backward(output, labels)
    model:backward(inputs, dloss_dw)

    -- return loss and gradients
    return loss,grad_params
end
log('Defined evaluation function!')

-- define a function for testing loss on test set
function test_loss(set)
    log('Testing on dataset of ' .. #dataset.test .. ' in batches of ' .. opt.batch_size)
    loader:set(dataset.test) -- initialise loader with the test set
    local loaded = 0
    local batch = 0
    local loss_sum = 0
    repeat
        -- load data into batch_inputs
        loaded, inputs, labels = loader:load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            -- force binary labels if set (any labels > 0 forced to 1)
            if opt.force_binary_labels then
                labels[labels:gt(0)] = 1
            end
            
            -- perform forward model and criterion pass
            local output = model:forward(inputs)
            local loss = criterion:forward(output, labels)
                
            -- accumulate loss
            loss_sum = loss_sum + loss
            batch = batch + 1
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    return loss_sum / batch
end
log('Defined test loss function')

-- perform training
losses = {}
test_losses = {}
local optim_state = {learningRate = opt.learning_rate}
log('Starting training')
log('Training on dataset of ' .. #dataset.train .. ' in batches of ' .. opt.batch_size .. ', ' .. opt.epochs .. ' epochs')
model.verbose = false

for epoch = 1, opt.epochs do
    
    -- perform iteration over training set
    loader:set(dataset.train) -- initialise loader with the training set
    local start_time = os.clock()
    
    local loaded = 0
    local batch = 0
    local loss_sum = 0
    repeat
        -- load data into batch_inputs
        loaded, inputs, labels = loader:load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            -- perform optimisation
            local _, loss = optim.adadelta(feval, params, {})
                
            -- record losses
            losses[#losses + 1] = loss[1]
            loss_sum = loss_sum + loss[1]
            batch = batch + 1
            if opt.batch_print_every > 0 and batch % opt.batch_print_every == 0 then
                log(string.format('Trained %5d batches (loss: %.3f)', batch, loss_sum / batch))
                collectgarbage()
            end
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    local training_time = os.clock() - start_time
    local epoch_loss = loss_sum / batch

    -- compute the loss over the test set
    start_time = os.clock()
    test_losses[#losses] = test_loss(dataset.test)
    local test_time = os.clock() - start_time
    
    if epoch % opt.print_every == 0 then
        log(string.format("Epoch %4d complete in %5.1fs / %5.1fs. Loss: %10.6f, Test Loss: %10.6f", epoch, training_time, test_time, epoch_loss, test_losses[#losses]))
        log(string.format("GPU Usage: %s", usageGPU('gb')))
        collectgarbage()
    end

    if epoch % opt.save_every == 0 then
      log('Saving model checkpoint ' .. epoch)
      local cache = cleanupModel(model)
      model:clearState()
      torch.save(job_dir .. 'model_' .. epoch .. '.t7', model)
      restoreModel(model, cache)
      log('Saving losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'losses_' .. epoch .. '.t7', losses)
      log('Saving test losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'test_losses_' .. epoch .. '.t7', test_losses)
    end
end

model = nil
criterion = nil
output = nil
inputs = nil
labels = nil
collectgarbage()
print('Final GPU Usage: ' .. usageGPU('gb'))

log('Done!')
