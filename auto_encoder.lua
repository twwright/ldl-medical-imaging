-- prepare print function (depending on whether log is defined)
if not log then
  log = print
end

-- -----------------
-- LOAD DEPENDENCIES
-- -----------------

-- make sure we have what we need
require 'cunn'
require 'nngraph'
require 'optim'
require 'mi_utils' -- helper functions for loading scans/labels

-- -------------------
-- PREPARE THE DATASET
-- -------------------


-- LOAD THE DATASET LISTING
log('Loading dataset')
local dataset_list = mi_list_dataset(opt.dataset_file)
-- apply filter to dataset_list
function filter(e)
    return e.slice_thickness == 2.5
end
dataset_list = mi_filter(dataset_list, filter)

-- Knuth shuffle the dataset
function shuffle(t)
    local n = #t
    while n > 1 do
        local k = math.random(n)
        t[n], t[k] = t[k], t[n]
        n = n - 1
    end
    return t
end
shuffle(dataset_list)

-- split dataset into training and test sets
dataset = {
    train = {},
    test = {},
    root_dir = opt.dataset_root
}
local dataset_size = math.min(#dataset_list, opt.max_dataset_size)
local train_last = math.floor(dataset_size * opt.dataset_split)
for i = 1, dataset_size do
    if i <= train_last then
        table.insert(dataset.train, dataset_list[i])
    else
        table.insert(dataset.test, dataset_list[i])
    end
end
log('Training dataset: ' .. #dataset.train)
log('Testing dataset: '.. #dataset.test)
log('Loaded dataset!')

-- CREATE STORAGE FOR INPUTS

-- clear up CUDA tensors if they exist
batch_inputs = nil
inputs = nil
collectgarbage()
log('Cleared CUDA resources')

-- create subsampling
local SubSampler = nn.VolumetricMaxPooling
if opt.subsampler == 'mean' then SubSampler = nn.VolumetricAveragePooling end
subsample = SubSampler(opt.subsample, opt.subsample, opt.subsample, opt.subsample, opt.subsample, opt.subsample)
subsample:float()

-- create tensors to load inputs and labels into
local size = opt.window_size * 2 + 1 -- middle slice plus window_size on each side
batch_inputs = torch.FloatTensor(opt.batch_size, 1, size, opt.size[1]/opt.subsample, opt.size[2]/opt.subsample)
batch_inputs = batch_inputs:cuda()

log('Storage created!')

-- DEFINE FUNCTIONS TO LOAD INPUTS
function load_input(dir_name, annotation_count)
    local scan = mi_load_scan(dir_name .. '/slices')

    -- scan
    local scan_input = torch.FloatTensor(1, scan.slices:size(1), scan.slices:size(2), scan.slices:size(2)):copy(scan.slices)
    local pooled_input = subsample:forward(scan_input)
    local padded_input = torch.FloatTensor(pooled_input:size(2) + (opt.window_size * 2), pooled_input:size(3), pooled_input:size(4)):zero()
    padded_input[{{opt.window_size + 1, pooled_input:size(2) + opt.window_size}}]:copy(pooled_input[1])
    return padded_input
end

function load_batch(p_batch, p_load, input)
    -- p_batch points to the last index that was filled in batch (p_batch + 1 is next spot to load)
    -- p_load points to the last index that was loaded from the input (p_load + 1 is start of next window to load)
    while p_batch < opt.batch_size and p_load + opt.window_size * 2 + 1 <= padded_input:size(1) do
        p_batch = p_batch + 1
        p_load = p_load + 1
        batch_inputs[p_batch]:copy(input[{{p_load, p_load + opt.window_size * 2}}])
    end
    return p_batch, p_load, input:size(1) - (p_load + opt.window_size * 2) -- second return is number of windows remaining in input
end

function dataset.init(set)
    p_dataset = 0
    p_load = 0 -- p_load points to the last index that was loaded from the input (p_load + 1 is start of next window to load)
    loaded_inputs = {}
    windows = {}
    dataset.active = set or dataset.train
end

-- initial state
function dataset.load()
    local p_batch = 0 -- p_batch points to the last index that was filled in batch (p_batch + 1 is next spot to load)
    -- while (batch not full) AND (dataset has data OR loaded input has data)
    while p_batch < opt.batch_size and (p_dataset < #dataset.active or p_load < #windows) do
        -- if loaded input exhausted
        if p_load == #windows then
            -- load input batch and create window list
            loaded_inputs = {}
            windows = {}
            for i = 1, opt.load_batch_size do
                p_dataset = p_dataset + 1
                if p_dataset > #dataset.active then
                    break
                end
                loaded_inputs[i] = load_input(dataset.root_dir .. dataset.active[p_dataset].name, dataset.active[p_dataset].annotation_count)
                for j = 1, loaded_inputs[i]:size(1) - (opt.window_size * 2) do -- add a entry to list for each window in input
                    table.insert(windows, {i, j})
                end
            end
            log(string.format('Loaded %d new windows (%d inputs)', #windows, #loaded_inputs))
            -- shuffle window list
            shuffle(windows)
            p_load = 0
        end
        -- fill batch from input
        while p_batch < opt.batch_size and p_load < #windows do
            p_batch = p_batch + 1
            p_load = p_load + 1
            local input = windows[p_load][1]
            local window = windows[p_load][2]
            batch_inputs[p_batch]:copy(loaded_inputs[input][{{window, window + opt.window_size * 2}}])
        end
        --log('Batch loaded to ' .. p_batch .. '/' .. opt.batch_size .. ', ' .. remaining .. ' remaining')
    end
    return p_batch
end

-- ----------------
-- CREATE THE MODEL
-- ----------------

-- clear up CUDA model and criterion if they exist
model = nil
criterion = nil
collectgarbage()
log('Cleared CUDA model resources')

-- input sizes
local in_depth = opt.window_size * 2 + 1
local in_height = opt.size[1] / opt.subsample
local in_width = opt.size[2] / opt.subsample

-- create the model
if opt.seed then
    torch.manualSeed(opt.seed)
end
model = nn.Sequential()

local maps = 1
local depth = in_depth
local height = in_height
local width = in_width
local nonlinearity = nn.Sigmoid
if opt.nonlinearity == 'relu' then nonlinearity = nn.ReLU end
if opt.nonlinearity == 'tanh' then nonlinearity = nn.Tanh end

-- start the nngraph model
local input_layer = nn.Identity()()
local prev_layer = input_layer

-- helper functions to add layers
function add_batch_norm(maps, d, h, w)
    model:add(nn.Reshape(maps, d * h, w))
    model:add(nn.SpatialBatchNormalization(maps))
    model:add(nn.Reshape(maps, d, h, w))
    --prev_layer = nn.Reshape(maps, d, h, w)(nn.SpatialBatchNormalization(maps)(nn.Reshape(maps, d * h, w)(prev_layer)))
end

function add_max_pool(kernel)
    --prev_layer = nn.VolumetricMaxPooling(kernel, kernel, kernel)(prev_layer)
    model:add(nn.VolumetricMaxPooling(kernel, kernel, kernel))
    depth = math.floor(depth / kernel)
    width = math.floor(width / kernel)
    height = math.floor(height / kernel)
end

function add_conv(out_maps, kern_d, kern_h, kern_w, norm)
    --prev_layer = nn.VolumetricConvolution(maps, out_maps, kern_d, kern_h, kern_w)(prev_layer)
    model:add(nn.VolumetricConvolution(maps, out_maps, kern_d, kern_h, kern_w))
    depth = depth - kern_d + 1
    height = height - kern_h + 1
    width = width - kern_w + 1
    if opt.batch_norm == true then
        add_batch_norm(out_maps, depth, height, width)
    end
    --prev_layer = nonlinearity()(prev_layer)
    model:add(nonlinearity())
    maps = out_maps
end

function add_deconv(out_maps, kern_d, kern_h, kern_w, stride_d, stride_h, stride_w, add_nonlinearity)
    stride_d = stride_d or 1
    stride_h = stride_h or 1
    stride_w = stride_w or 1
    add_nonlinearity = add_nonlinearity or true
    --prev_layer = nn.VolumetricFullConvolution(maps, out_maps, kern_d, kern_h, kern_w, stride_d, stride_h, stride_w)(prev_layer)
    model:add(nn.VolumetricFullConvolution(maps, out_maps, kern_d, kern_h, kern_w, stride_d, stride_h, stride_w))
    depth = (depth - 1) * stride_d + kern_d
    height = (height - 1) * stride_h + kern_h
    width = (width - 1) * stride_w + kern_w
    if opt.batch_norm == true then
        add_batch_norm(out_maps, depth, height, width)
    end
    if add_nonlinearity then
        --prev_layer = nonlinearity()(prev_layer)
        model:add(nonlinearity())
    end
    maps = out_maps
end

-- create layers as defined in opts
for i, l in ipairs(opt.model) do
    if l.type == 'conv' then
        add_conv(l.maps, l.kernel[1], l.kernel[2], l.kernel[3])
    elseif l.type == 'pool' then
        add_max_pool(l.kernel)
    elseif l.type == 'deconv' then
        local stride = l.stride or {1, 1, 1}
        add_deconv(l.maps, l.kernel[1], l.kernel[2], l.kernel[3], stride[1], stride[2], stride[3])
    end
end

-- add deconvolution layers as decoder
local kernel = {
    in_depth - depth + 1,
    in_height - height + 1,

    in_width - width + 1
}
add_deconv(1, kernel[1], kernel[2], kernel[3], false)
log(depth, height, width)
prev_layer = nn.Sigmoid()(prev_layer)

-- finalise model
--model = nn.gModule({input_layer}, {prev_layer})
model:cuda()

-- create the criterion
criterion = nn.MSECriterion()
criterion:cuda()

-- get the model parameters
params, grad_params = model:getParameters()

log('Model and criterion created!')

-- test the model sizes
log('Inputs')
print(batch_inputs:size())
model.verbose = true
local output = model:forward(batch_inputs)
-- output sizes
local sizes = torch.IntTensor(batch_inputs:dim(), model:size()):fill(0)
for i = 1, model:size() do
    local outputSize = model:get(i).output:size():size()
    for j = 1, outputSize do
        sizes[j][i] = model:get(i).output:size()[j]
    end
end
model.verbose = false
print(sizes)
output = nil

-- ------------------
-- TRAINING THE MODEL
-- ------------------

-- define the evaluation function
function feval(x)
    --log('Running feval..')
    if x ~= params then
        params:copy(x)
    end

    -- reset gradients
    grad_params:zero()

    -- evaluate training batch
    local output = model:forward(inputs)
    local loss = criterion:forward(output, inputs)

    -- estimate dloss/dW
    local dloss_dw = criterion:backward(output, inputs)
    model:backward(inputs, dloss_dw)

    -- return loss and gradients
    return loss,grad_params
end
log('Defined evaluation function!')

-- define a function for testing loss on test set
function test_loss(set)
    log('Testing on dataset of ' .. #dataset.test .. ' in batches of ' .. opt.batch_size)
    dataset.init(dataset.test) -- initialise loader with the test set
    local loaded = 0
    local batch = 0
    local loss_sum = 0
    repeat
        -- load data into batch_inputs
        loaded = dataset.load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            inputs = batch_inputs[{{1, loaded}}]

            -- perform whitening
            
            local mean = opt.whiten_mean
            local std = opt.whiten_std
            inputs:add(-mean):div(std)
            
            -- perform forward and backward pass
            local output = model:forward(inputs)
            local loss = criterion:forward(output, inputs)
                
            -- accumulate loss
            loss_sum = loss_sum + loss
            batch = batch + 1
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    return loss_sum / batch
end
log('Defined test loss function')

-- perform training
losses = {}
test_losses = {}
local optim_state = {learningRate = opt.learning_rate}
log('Starting training')
log('Training on dataset of ' .. #dataset.train .. ' in batches of ' .. opt.batch_size .. ', ' .. opt.epochs .. ' epochs')
model.verbose = false

for epoch = 1, opt.epochs do
    
    -- perform iteration over training set
    dataset.init(dataset.train) -- initialise loader with the training set
    local start_time = os.clock()
    
    local loaded = 0
    local batch = 0
    repeat
        -- load data into batch_inputs
        loaded = dataset.load()
        
        -- set input and label tensor as subset of batch batch that is filled
        if loaded > 0 then
            inputs = batch_inputs[{{1, loaded}}]

            -- perform whitening
            local mean = opt.whiten_mean
            local std = opt.whiten_std
            inputs:add(-mean):div(std)
            
            -- perform optimisation
            local _, loss = optim.sgd(feval, params, optim_state)
                
            -- record losses
            losses[#losses + 1] = loss[1]
            batch = batch + 1
            if opt.batch_print_every > 0 and batch % opt.batch_print_every == 0 then
                log(string.format('Trained %5d batches', batch))
                collectgarbage()
            end
            
            -- display the result of the auto-encoder
            if batch % (opt.batch_print_every * 2) == 0 then
                --itorch.image(inputs[{1, 1, 5}])
                --itorch.image(model:forward(inputs)[{1, 1, 5}])
            end
        end
    until loaded == 0 or (opt.max_batches > 0 and batch == opt.max_batches)
    local training_time = os.clock() - start_time
    
    -- compute the loss over the test set
    start_time = os.clock()
    test_losses[#losses] = test_loss(dataset.test)
    local test_time = os.clock() - start_time
    
    if epoch % opt.print_every == 0 then
        log(string.format("Epoch %4d complete in %5.1fs / %5.1fs. Loss: %10.6f, Test Loss: %10.6f", epoch, training_time, test_time, losses[#losses], test_losses[#losses]))
        log(string.format("GPU Usage: %s", usageGPU('gb')))
        collectgarbage()
    end

    if epoch % opt.save_every == 0 then
      log('Saving model checkpoint ' .. epoch)
      local cache = cleanupModel(model)
      torch.save(job_dir .. 'model_' .. epoch .. '.t7', model)
      restoreModel(model, cache)
      log('Saving losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'losses_' .. epoch .. '.t7', losses)
      log('Saving test losses checkpoint ' .. epoch)
      torch.save(job_dir .. 'test_losses_' .. epoch .. '.t7', test_losses)
    end
end

model = nil
criterion = nil
batch_inputs = nil
loaded_inputs = nil
windows = nil
output = nil
inputs = nil
collectgarbage()
print('Final GPU Usage: ' .. usageGPU('gb'))

log('Done!')
