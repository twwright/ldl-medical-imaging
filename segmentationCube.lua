-- prepare print function (depending on whether log is defined)
if not log then
  log = print
end

-- -----------------
-- LOAD DEPENDENCIES
-- -----------------

-- make sure we have what we need
require 'nn'
--require 'cunn'
require 'nngraph'
require 'optim'
local nninit = require 'nninit'

require 'ldl-medical-imaging.adadelta_wd' -- custom adadelta that has weight decay added
require 'ldl-medical-imaging.WeightedBCECriterion' -- custom weighted binary cross-entropy criterion
require 'ldl-medical-imaging.mi_utils' -- helper functions for loading scans/labels
 
-- set the random seed
if opt.seed then
  torch.manualSeed(opt.seed)
else
  opt.seed = torch.seed()
end

-- -------------------
-- PREPARE THE DATASET
-- -------------------

-- LOAD THE DATASET LISTING
log('Loading dataset')
local data = torch.load(opt.nodule_file)
local nodules = {}
for _, d in ipairs(data) do
    if d.slice_thickness == opt.base_thickness then
        for _, n in ipairs(d.nodules) do
            -- force binary labels if set (any labels > 0 forced to 1)
            if opt.force_binary_labels then
                n.label[n.label:gt(0.5)] = 1
                n.label[n.label:le(0.5)] = 0
            end
            table.insert(nodules, n)
        end
    end
end
-- Knuth shuffle the datasets
if opt.shuffle_datasets then
    shuffle(nodules)
end

-- split dataset into training and test sets
local dataset = {
    train = {},
    test = {}
}
local dataset_size = #nodules
local train_last = math.floor(dataset_size * opt.dataset_split)
for i = 1, dataset_size do
    if i <= train_last then
        table.insert(dataset.train, nodules[i])
    else
        table.insert(dataset.test, nodules[i])
    end
end

log('Training dataset: ' .. #dataset.train)
log('Testing dataset: '.. #dataset.test)
log('Loaded dataset!')

-- CREATE STORAGE FOR INPUTS
local inputSize = dataset.train[1].scan:size()
local batch_input = torch.FloatTensor(opt.batch_size, 1, inputSize[1], inputSize[2], inputSize[3])
local labelSize = dataset.train[1].label:size()
local batch_label = torch.FloatTensor(opt.batch_size, 1, labelSize[1], labelSize[2], labelSize[3])
local inputs, labels

function loadBatch(opt, dataset, batchIndex)
    local batchStart = ((batchIndex - 1) * opt.batch_size) + 1
    local batchEnd = math.min(batchIndex * opt.batch_size, #dataset)
    local batchSize = batchEnd - batchStart + 1
    for i = batchStart, batchEnd do
        batch_input[i - batchStart + 1]:copy(dataset[i].scan)
        batch_label[i - batchStart + 1]:copy(dataset[i].label)
    end
    return batchSize, batch_input[{{i, batchSize}}], batch_label[{{i, batchSize}}]
end

-- ----------------
-- CREATE THE MODEL
-- ----------------

-- clear up CUDA model and criterion if they exist
model = nil
criterion = nil
collectgarbage()
log('Cleared CUDA model resources')

local Seq = nn.Sequential
local ReLU = nn.ReLU
local VolBatchNorm = nn.VolumetricBatchNormalization

local function VolConv(...)
  return nn.VolumetricConvolution(...)
    :init('weight', nninit.kaiming, {dist = 'normal', gain = 'relu'})
    :init('bias', nninit.constant, 0)
end

local function VolFullConv(...)
  return nn.VolumetricFullConvolution(...)
end

if opt.skipModel then
    
    local input = nn.Identity()()
    local layers = {
        --{ layer = input, maps = 1 }
    }
    local maps = opt.maps or 32
    
    local last = ReLU(true)(VolBatchNorm(maps)(VolConv(1,maps, 3,3,3, 1,1,1, 1,1,1)(input)))
    table.insert(layers, { layer = last, maps = maps } )
    
    -- create layers downwards, saving each level of dimensionality
    for i = 1, 3 do
        -- Conv
        last = ReLU(true)(VolBatchNorm(maps)(VolConv(maps,maps, 3,3,3, 1,1,1, 1,1,1)(last)))
        
        -- Conv Down
        local oldMaps = maps
        maps = maps * 2
        last = ReLU(true)(VolBatchNorm(maps)(VolConv(oldMaps,maps, 3,3,3, 2,2,2, 1,1,1)(last)))
        
        table.insert(layers, { layer = last, maps = maps })
    end
    
    -- for each layer that is going to be involved in final prediction, produce a prediction for that layer
    for i, layer in ipairs(layers) do
        layer.layer = VolConv(layer.maps,1, 1,1,1, 1,1,1, 0,0,0)(layer.layer)
    end
        
    -- create layers combining upwards to create prediction
    last = layers[#layers].layer
    for i = #layers-1, 1, -1 do
        local layer = layers[i].layer
        
        -- upsample previous layer and add with current layer
        last = nn.CAddTable()({
            layer,
            VolFullConv(1,1,  2,2,2,  2,2,2,  0,0,0)(last)
        })
    end
    
    local output = nn.Sigmoid()(last)
    
    model = nn.gModule({input}, {output})

else
    
    local function Conv()
      model
        :add(VolConv(state.maps, state.maps, 3,3,3, 1,1,1, 1,1,1))
        :add(VolBatchNorm(state.maps))
        :add(ReLU(true))
    end
    
    local function FullConv()
      model
        :add(VolFullConv(state.maps, state.maps, 3,3,3, 1,1,1, 1,1,1))
        :add(VolBatchNorm(state.maps))
        :add(ReLU(true))
    end

    local function ConvDown()
      model
        :add(VolConv(state.maps, state.maps * 2, 3,3,3, 2,2,2, 1,1,1))
        :add(VolBatchNorm(state.maps * 2))
        :add(ReLU(true))
      state.maps = state.maps * 2
    end

    local function ConvUp()
      model
        :add(VolFullConv(state.maps, state.maps / 2, 2,2,2, 2,2,2, 0,0,0))
        :add(VolBatchNorm(state.maps / 2))
        :add(ReLU(true))
      state.maps = state.maps / 2
    end
    
    state = {
      maps = opt.maps or 32
    }

    model = Seq() -- 1 x 64 x 64 x 64
    
    if opt.convCount > 0 then
      model
        :add(VolConv(1, state.maps,  3, 3, 3,  1, 1, 1,  1, 1, 1)) -- 32 x 32 x 32 x 32
        :add(VolBatchNorm(state.maps))
        :add(ReLU(true))
        
      ConvDown()
    else
      model
        :add(VolConv(1, state.maps,  3, 3, 3,  2, 2, 2,  1, 1, 1)) -- 32 x 32 x 32 x 32
        :add(VolBatchNorm(state.maps))
        :add(ReLU(true))
    end

    
    for i = 1, opt.convCount do Conv() end
    ConvDown() -- 64 x 16 x 16 x 16

    for i = 1, opt.convCount do Conv() end
    ConvDown() -- 128 x 8 x 8 x 8

    --Conv()
    --ConvDown() -- 256 x 4 x 4 x 4

    --Conv()
    --ConvUp() -- 128 x 8 x 8 x 8

    for i = 1, opt.convCount do Conv() end
    ConvUp() -- 64 x 16 x 16 x 16

    for i = 1, opt.convCount do Conv() end
    ConvUp() -- 32 x 32 x 32 x 32

    for i = 1, opt.convCount do Conv() end
    model:add(VolFullConv(state.maps, 1,  2, 2, 2,  2, 2, 2,  0, 0, 0)) -- 1 x 64 x 64 x 64
    model:add(nn.Sigmoid())
    
end

model:float()
local criterion = nn.WeightedBCECriterion(opt.bce_weight or 0.5):float()
log('Criterion: ' .. torch.type(criterion))

-- activate cuda stuff
if opt.gpu then
    require 'cunn'
    log('GPU-acceleration enabled')
    model:cuda()
    criterion:cuda()
    batch_input = batch_input:cuda()
    batch_label = batch_label:cuda()
end
log('Model and criterion created!')

-- get the model parameters
params, grad_params = model:getParameters()

-- test the model sizes
log('Inputs')
print(batch_input:size())
local output = model:forward(batch_input)
-- output sizes
local sizes = torch.IntTensor(batch_input:dim(), model:size()):fill(0)
for i = 1, model:size() do
    local outputSize = model:get(i).output:size():size()
    for j = 1, outputSize do
        sizes[j][i] = model:get(i).output:size()[j]
    end
end
print(sizes)
output = nil

-- ------------------
-- TRAINING THE MODEL
-- ------------------

-- define the evaluation function
function feval(x)
    --log('Running feval..')
    if x ~= params then
        params:copy(x)
    end

    -- reset gradients
    grad_params:zero()

    -- evaluate training batch
    local output = model:forward(inputs)
    local loss = criterion:forward(output, labels)

    -- estimate dloss/dW
    local dloss_dw = criterion:backward(output, labels)
    model:backward(inputs, dloss_dw)

    -- return loss and gradients
    return loss,grad_params
end
log('Defined evaluation function!')

-- define a function for testing loss on test set
function test_loss(set)
    log('Testing on dataset of ' .. #set .. ' in batches of ' .. opt.batch_size)
    local numBatches = math.ceil(#dataset.test / opt.batch_size)
    local lossSum = 0
    for i = 1, numBatches do
        batchSize, inputs, labels = loadBatch(opt, set, i)
        
        -- perform forward model and criterion pass
        local output = model:forward(inputs)
        local loss = criterion:forward(output, labels)

        -- accumulate loss
        lossSum = lossSum + loss
        if opt.batch_print_every > 0 and i % opt.batch_print_every == 0 then
            log(string.format('Tested %5d batches (loss: %.3f)', i, lossSum / i))
            collectgarbage()
        end
    end
    return lossSum / numBatches
end
log('Defined test loss function')

-- define a function for evaluating performance
function evaluate(set)
    log('Evaluating performance on dataset of ' .. #set .. ' in batches of ' .. opt.batch_size)
    local numBatches = math.ceil(#dataset.test / opt.batch_size)
    local time = os.time()
    -- Prepare evaluation
    local num_thresholds = opt.pr_thresholds or 25
    local thresholds = torch.linspace(0, 1, num_thresholds)
    local results = torch.FloatTensor(thresholds:size(1), 9):zero()
    for i = 1, numBatches do
        batchSize, inputs, labels = loadBatch(opt, set, i) 

        -- perform forward model and criterion pass
        local output = model:forward(inputs)

        local y = labels:gt(0.5):float()
        for k = 1, thresholds:size(1) do
            local y_hat = output:ge(thresholds[k]):float()
            y_hat:mul(2):add(y)
            results[k][1] = results[k][1] + y_hat:eq(3):sum() -- true positive: 1x2 + 1
            results[k][2] = results[k][2] + y_hat:eq(2):sum() -- false positive: 1x2 + 0
            results[k][3] = results[k][3] + y_hat:eq(0):sum() -- true negative: 0x2 + 0
            results[k][4] = results[k][4] + y_hat:eq(1):sum() -- false negative: 0x2 + 1
        end
    end
    
    -- Calculate metrics
    for j = 1, thresholds:size(1) do
        local tp = results[j][1]
        local fp = results[j][2]
        local tn = results[j][3]
        local fn = results[j][4]
        if fp == 0 then
            results[j][5] = 0
        else
            results[j][5] = tp / (tp + fp) -- precision = tp / (tp + fp)
        end
        results[j][6] = tp / (tp + fn) -- recall = tp / (tp + fn)
        results[j][7] = fp / (fp + tn) -- fpr
        results[j][8] = (tp + tn) / (tp + fp + tn + fn) -- accuracy
        results[j][9] = 2 * ((results[j][5] * results[j][6]) / (results[j][5] + results[j][6]))
        
        log(string.format('%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%d\t%d\t%d\t%d\t',
                results[j][9],
                results[j][5],
                results[j][6],
                results[j][7],
                results[j][8],
                results[j][1],
                results[j][2],
                results[j][3],
                results[j][4]
        ))
    end

    log(string.format('Evaluation complete in %.2fs', os.time() - time))
    
    return results
end

-- perform training
local losses = {}
local epoch_losses = {}
local test_losses = {}
local config = { weightDecay = opt.weight_decay or 0 }
log('Starting training')
log('Training on dataset of ' .. #dataset.train .. ' in batches of ' .. opt.batch_size .. ', ' .. opt.epochs .. ' epochs')
model.verbose = false

for epoch = 1, opt.epochs do
    
    -- shuffle the datasets again
    if opt.shuffle_datasets then
        shuffle(dataset.train)
        shuffle(dataset.test)
    end
    
    -- perform iteration over training set
    local start_time = os.time()
    local loss_sum = 0
    local numBatches = math.ceil(#dataset.train / opt.batch_size)
    for i = 1, numBatches do
        batchSize, inputs, labels = loadBatch(opt, dataset.train, i)

        -- perform optimisation
        local _, loss = optim.adadeltawd(feval, params, config)

        -- record losses
        losses[#losses + 1] = loss[1]
        loss_sum = loss_sum + loss[1]
        
        if opt.batch_print_every > 0 and i % opt.batch_print_every == 0 then
            log(string.format('Trained %5d batches (loss: %.3f)', i, loss_sum / i))
            collectgarbage()
        end
    end
    
    local training_time = os.time() - start_time
    local epoch_loss = loss_sum / numBatches
    epoch_losses[epoch] = epoch_loss

    -- compute the loss over the test set
    start_time = os.time()
    test_losses[epoch] = test_loss(dataset.test)
    local test_time = os.time() - start_time
    
    -- evaluate performance on the test set
    local evaluation
    if opt.evaluate and (epoch % opt.save_every == 0 or epoch == opt.epochs) then
        evaluation = evaluate(dataset.test)
    end
    
    if epoch % opt.print_every == 0 then
        log(string.format("Epoch %4d complete in %5.1fs / %5.1fs. Loss: %10.6f, Test Loss: %10.6f", epoch, training_time, test_time, epoch_loss, test_losses[epoch]))
        --log(string.format("GPU Usage: %s", usageGPU('gb')))
        collectgarbage()
    end

    if epoch % opt.save_every == 0 or epoch == opt.epochs then
      log('Saving checkpoint ' .. epoch)
      --local cache = cleanupModel(model)
      model:clearState()
      torch.save(job_dir .. 'model.t7', {
          opt = opt,
          model = model,
          losses = losses,
          epoch_losses = epoch_losses,
          test_losses = test_losses
      })
      --restoreModel(model, cache)
      if opt.evaluate then
          torch.save(job_dir .. 'evaluation_' .. epoch .. '.t7', evaluation)
      end
      collectgarbage()
    end
end

model = nil
criterion = nil
output = nil
inputs = nil
labels = nil
collectgarbage()
print('Final GPU Usage: ' .. usageGPU('gb'))

log('Done!')