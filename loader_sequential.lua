-- CREATE STORAGE FOR INPUTS
local loader = {}

function loader:init(opt)
    self.opt = opt
        
        -- create subsampling
    local input_sub = opt.input_subsample or 1
    local label_sub = opt.label_subsample or 1
    self.mean_subsample = nn.VolumetricAveragePooling(input_sub, input_sub, input_sub, input_sub, input_sub, input_sub)
    self.mean_subsample:float()
    -- labels will be subsampled by input_sub in depth dimension, and label_sub in other dimensions!
    self.max_subsample = nn.VolumetricMaxPooling(input_sub, label_sub, label_sub, input_sub, label_sub, label_sub)
    self.max_subsample:float()

    -- calculate crops
    self.total_crops = (2 * opt.crops - 1) * (2 * opt.crops - 1)
    
    -- create tensors to load inputs and labels into
    local depth = opt.window_size * 2 + 1 -- middle slice plus window_size on each side
    local input_size = (opt.size / opt.input_subsample) / opt.crops
    self.batch_inputs = torch.FloatTensor(opt.batch_size, 1, depth, input_size, input_size)
    self.batch_inputs = self.batch_inputs:cuda()
    if self.opt.load_labels then
        local label_size = (opt.size / opt.label_subsample) / opt.crops
        self.batch_labels = torch.FloatTensor(opt.batch_size, 1, 1, label_size, label_size)
        self.batch_labels = self.batch_labels:cuda()
    end
end

-- DEFINE FUNCTIONS TO LOAD INPUTS
function loader:load_input(dir_name, annotation_count)
    -- load the scan, calculate the slice copying factor (every slice, every second, every fourth etc.) and generate copy indices
    local scan = mi_load_scan(dir_name .. '/slices')
    local factor = self.opt.base_thickness / scan.slice_thickness
    local indices = torch.range(1, scan.slices:size(1), factor)
    
    -- scan, copy correct slices and mean subsample
    local scan_input = torch.FloatTensor(1, math.ceil(scan.slices:size(1) / factor), scan.slices:size(2), scan.slices:size(2))
    if factor == 1 then
        scan_input:copy(scan.slices)
    else 
        for i = 1, math.ceil(scan.slices:size(1) / factor) do
            scan_input[1][i]:copy(scan.slices[indices[i]])
        end
    end
    local pooled_input = self.mean_subsample:forward(scan_input)
    local padded_input = torch.FloatTensor(pooled_input:size(2) + (self.opt.window_size * 2), pooled_input:size(3), pooled_input:size(4)):zero()
    padded_input[{{self.opt.window_size + 1, pooled_input:size(2) + self.opt.window_size}}]:copy(pooled_input[1])
    
    -- label, copy correct slices, divide by annotation count to create average, max subsample
    if self.opt.load_labels then
        local label = mi_load_annotation(dir_name .. '/annotations_sum')
        local label_output = torch.FloatTensor(1, math.ceil(label.slices:size(1) / factor), label.slices:size(2), label.slices:size(2))
        if factor == 1 then
            label_output:copy(label.slices)
        else 
            for i = 1, math.ceil(label.slices:size(1) / factor) do
                label_output[1][i]:copy(label.slices[indices[i]])
            end
        end
        label_output:div(annotation_count)
        local pooled_output = self.max_subsample:forward(label_output)
        return padded_input, pooled_output[1]:clone() -- subsampling required a batch dimension, we don't want that
    end
    return padded_input -- if we reach here load_labels must be false
end

-- assign a new dataset to the loader
function loader:set(set)
    self.p_dataset = 0
    self.p_load = 0 -- p_load points to the last index that was loaded from the input (p_load + 1 is start of next window to load)
    self.dataset = set
    self.loaded = nil
end

-- load batches from the 
function loader:load()    
    local p_batch = 0 -- p_batch points to the last index that was filled in batch (p_batch + 1 is next spot to load)
    -- while (batch not full) AND loaded input has data
    while p_batch < self.opt.batch_size and (self.loaded and self.p_load < (self.loaded.input:size(1) - self.opt.window_size*2) * self.total_crops) do
        p_batch = p_batch + 1
        self.p_load = self.p_load + 1
        -- determine which window we want
        local depth = math.ceil(self.p_load / self.total_crops)
        local crop_index = (self.p_load % self.total_crops) - 1 -- calculating a 0-based index
        if crop_index == -1 then crop_index = (self.total_crops - 1) end -- need to wrap-around the last index
        -- calculate crop dimensions
        local crop_size = self.loaded.input:size(2) / self.opt.crops
        local crop_x = (crop_index % (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
        local crop_y = math.floor(crop_index / (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
        --print(string.format('[{%d, %d}, {%d, %d}, {%d, %d}]', depth, depth + self.opt.window_size * 2, crop_y + 1, crop_y + crop_size, crop_x + 1, crop_x + crop_size))
        -- copy batch element
        self.batch_inputs[p_batch]:copy(self.loaded.input[{
                    {depth, depth + self.opt.window_size * 2},
                    {crop_y + 1, crop_y + crop_size},
                    {crop_x + 1, crop_x + crop_size}}])
        if self.opt.load_labels then
            crop_size = self.loaded.label:size(2) / self.opt.crops
            crop_x = (crop_index % (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
            crop_y = math.floor(crop_index / (2 * self.opt.crops - 1)) * (crop_size / 2) -- stride of half the crop_size
            self.batch_labels[p_batch]:copy(self.loaded.label[{depth, {crop_y + 1, crop_y + crop_size}, {crop_x + 1, crop_x + crop_size}}])
        end
    end
    
    if p_batch > 0 then
        if self.opt.load_labels then
            return p_batch, self.batch_inputs[{{1, p_batch}}], self.batch_labels[{{1, p_batch}}]
        else
            return p_batch, self.batch_inputs[{{1, p_batch}}]
        end
    else
        return 0
    end
end

function loader:next(index)
    -- allow for user to specify load index, or increment
    self.p_dataset = index or (self.p_dataset + 1)
    -- if this is not a valid index, exit
    if self.p_dataset >= #self.dataset then
        return 0
    end
    log('Loading 1 inputs...')
    local input, label = self:load_input(self.opt.dataset_root .. self.dataset[self.p_dataset].name, self.dataset[self.p_dataset].annotation_count)
    self.loaded = { input = input, label = label }
    log(string.format('Loaded %d new windows (1 inputs)', self.loaded.label:size(1) * self.total_crops))
    self.p_load = 0
    return self.loaded.input:size(1) - self.opt.window_size * 2
end

return loader